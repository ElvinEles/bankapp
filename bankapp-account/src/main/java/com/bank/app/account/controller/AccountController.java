package com.bank.app.account.controller;

import com.bank.app.account.dto.AccountDto;
import com.bank.app.account.dto.CreateAccountDto;
import com.bank.app.account.service.impl.AccountServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v2/account")
public class AccountController {

    private final AccountServiceImpl accountService;

    @GetMapping("/{id}")
    public ResponseEntity<AccountDto> getAccount(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(accountService.getAccount(id));
    }

    @GetMapping
    public ResponseEntity<List<AccountDto>> getAccounts() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(accountService.getAll());
    }

    @PostMapping
    public ResponseEntity<AccountDto> createAccount(@Valid @RequestBody CreateAccountDto dto) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(accountService.createAccount(dto));
    }


}
