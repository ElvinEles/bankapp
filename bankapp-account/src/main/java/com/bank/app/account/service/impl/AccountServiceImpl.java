package com.bank.app.account.service.impl;

import com.bank.app.account.dto.AccountDto;
import com.bank.app.account.dto.CreateAccountDto;

import java.util.List;

public interface AccountServiceImpl {

    AccountDto getAccount(Long id);

    List<AccountDto> getAll();

    AccountDto createAccount(CreateAccountDto dto);
}
