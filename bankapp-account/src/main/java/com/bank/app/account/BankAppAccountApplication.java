package com.bank.app.account;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@EnableCaching
@EnableFeignClients
public class BankAppAccountApplication {

    public static void main(String[] args) {
        SpringApplication.run(BankAppAccountApplication.class, args);
    }

}
