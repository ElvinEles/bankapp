package com.bank.app.account.service;

import com.bank.app.account.dto.AccountDto;
import com.bank.app.account.dto.CreateAccountDto;
import com.bank.app.account.dto.UserDto;
import com.bank.app.account.exception.AccountNotFoundException;
import com.bank.app.account.feign.UserClient;
import com.bank.app.account.mapper.AccountMapper;
import com.bank.app.account.model.Account;
import com.bank.app.account.repository.AccountRepository;
import com.bank.app.account.service.impl.AccountServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;


@Service
@RequiredArgsConstructor
public class AccountService implements AccountServiceImpl {

    private final AccountRepository accountRepository;
    private final AccountMapper accountMapper;
    private final UserClient userClient;

    @Cacheable(cacheNames = "account", key = "#id")
    @Override
    public AccountDto getAccount(Long id) {
        return accountMapper.modelToDto(accountRepository.findById(id)
                .orElseThrow(AccountNotFoundException::new));
    }

    @Cacheable(cacheNames = "accounts")
    @Override
    public List<AccountDto> getAll() {
        return accountRepository.findAll()
                .stream()
                .map(accountMapper::modelToDto)
                .toList();
    }

    @Override
    public AccountDto createAccount(CreateAccountDto dto) {

        UserDto user = userClient.getUser(dto.customerId())
                .orElseThrow(() -> new RuntimeException("user is not found"));


        Account account = Account.builder()
                .name(dto.name())
                .createdTime(Instant.now())
                .customerId(user.id())
                .createdAt(Instant.now())
                .updatedAt(Instant.now())
                .build();

        return accountMapper.modelToDto(accountRepository.save(account));
    }
}


















