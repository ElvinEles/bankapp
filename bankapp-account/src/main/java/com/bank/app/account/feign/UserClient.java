package com.bank.app.account.feign;

import com.bank.app.account.dto.UserDto;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@FeignClient(name = "${feign.name}", url = "${feign.user.url}", fallback = Fallback.class)
public interface UserClient {

    @GetMapping("/auth/{id}")
    Optional<UserDto> getUser(@PathVariable Long id);
}
