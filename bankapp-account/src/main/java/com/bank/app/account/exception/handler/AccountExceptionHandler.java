package com.bank.app.account.exception.handler;


import com.bank.app.account.exception.AccountNotFoundException;
import com.bank.app.account.internalization.ResponseMessageSource;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
@RequiredArgsConstructor
public class AccountExceptionHandler {

    private static final String EXCEPTION_ACCOUNT_NOT_FOUND = "exception.account.notfound";
    private final ResponseMessageSource messageSource;

    @ExceptionHandler(AccountNotFoundException.class)
    public ResponseEntity<Map<String, Object>> accountNotFound(AccountNotFoundException exception) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(responseEx(messageSource.toLocale(EXCEPTION_ACCOUNT_NOT_FOUND), 400));
    }

    private Map<String, Object> responseEx(String message, int code) {
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put("timestamp", Instant.now().toString());
        responseMap.put("status", code);
        responseMap.put("error", message);
        return responseMap;
    }


}
















