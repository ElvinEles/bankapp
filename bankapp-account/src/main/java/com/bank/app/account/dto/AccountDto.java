package com.bank.app.account.dto;

import lombok.Builder;

import javax.validation.constraints.NotBlank;
import java.time.Instant;

@Builder
public record AccountDto(

        @NotBlank
        Long id,

        @NotBlank
        String name,


        Instant createdTime,

        @NotBlank
        Long customerId,

        Instant createdAt,

        Instant updatedAt
) {
}
