package com.bank.app.account.validation.execution;

import com.bank.app.account.validation.annotation.NotEmptyLong;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class LongTypeValidator implements ConstraintValidator<NotEmptyLong, Long> {
    @Override
    public boolean isValid(Long value, ConstraintValidatorContext context) {
        return value != null;
    }
}


