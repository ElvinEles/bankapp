package com.bank.app.account.dto;

import com.bank.app.account.validation.annotation.NotEmptyLong;
import lombok.Builder;

import javax.validation.constraints.NotBlank;

@Builder
public record CreateAccountDto(

        @NotBlank
        String name,

        @NotEmptyLong
        Long customerId
) {
}
