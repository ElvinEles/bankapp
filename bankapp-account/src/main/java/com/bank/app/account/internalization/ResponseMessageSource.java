package com.bank.app.account.internalization;

import lombok.RequiredArgsConstructor;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Component
@RequiredArgsConstructor
public class ResponseMessageSource {

    private final ResourceBundleMessageSource messageSource;

    public String toLocale(String msgcode) {
        Locale locale = LocaleContextHolder.getLocale();
        return messageSource.getMessage(msgcode, null, locale);
    }
}



