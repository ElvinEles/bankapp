package com.bank.app.account.mapper;

import com.bank.app.account.dto.AccountDto;
import com.bank.app.account.model.Account;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface AccountMapper {

    AccountDto modelToDto(Account account);

    Account dtoToModel(AccountDto dto);

}



