package com.bank.app.account.feign;

import com.bank.app.account.dto.UserDto;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class Fallback implements UserClient{

    @Override
    public Optional<UserDto> getUser(Long id) {
        throw new RuntimeException("boom");
    }
}
