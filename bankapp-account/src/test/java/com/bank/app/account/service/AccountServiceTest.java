package com.bank.app.account.service;

import com.bank.app.account.dto.AccountDto;
import com.bank.app.account.dto.CreateAccountDto;
import com.bank.app.account.exception.AccountNotFoundException;
import com.bank.app.account.mapper.AccountMapper;
import com.bank.app.account.model.Account;
import com.bank.app.account.repository.AccountRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@ExtendWith({MockitoExtension.class})
class AccountServiceTest {

    @Mock
    private AccountRepository accountRepository;
    @Mock
    private AccountMapper accountMapper;
    @InjectMocks
    private AccountService accountService;


    @Test
    void whenGetAccountByIdThenSuccess() {
        //getting data from db
        Account account = Account.builder()
                .id(1L)
                .name("credit")
                .createdTime(Instant.now())
                .customerId(2L)
                .createdAt(Instant.now())
                .updatedAt(Instant.now().plusSeconds(2000))
                .build();

        when(accountRepository.findById(anyLong())).thenReturn(Optional.of(account));

        AccountDto dto = AccountDto.builder()
                .id(account.getId())
                .name(account.getName())
                .createdTime(account.getCreatedTime())
                .customerId(account.getCustomerId())
                .createdAt(account.getCreatedAt())
                .updatedAt(account.getUpdatedAt())
                .build();

        //mapping
        when(accountMapper.modelToDto(any())).thenReturn(dto);


        AccountDto accountTest = accountService.getAccount(anyLong());

        //verify

        assertThat(accountTest.id()).isEqualTo(1L);
    }

    @Test
    void whenGetAccountByIdNotFoundThenReturnException() {
        when(accountRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThatThrownBy(() -> accountService.getAccount(5L))
                .isInstanceOf(AccountNotFoundException.class);

    }

    @Test
    void whenGetAccountsThenSuccess() {

        Account dto1 = Account.builder()
                .id(1L)
                .name("credit")
                .createdTime(Instant.now())
                .customerId(2L)
                .createdAt(Instant.now())
                .updatedAt(Instant.now().plusSeconds(2000))
                .build();

        Account dto2 = Account.builder()
                .id(2L)
                .name("credit1")
                .createdTime(Instant.now())
                .customerId(3L)
                .createdAt(Instant.now())
                .updatedAt(Instant.now().plusSeconds(2000))
                .build();

        when(accountRepository.findAll()).thenReturn(List.of(dto1, dto2));


        AccountDto dtoMapper = AccountDto.builder()
                .id(2L)
                .name("credit1")
                .createdTime(Instant.now())
                .customerId(3L)
                .createdAt(Instant.now())
                .updatedAt(Instant.now().plusSeconds(2000))
                .build();

        when(accountMapper.modelToDto(any())).thenReturn(dtoMapper);

        List<AccountDto> all = accountService.getAll();

        assertEquals(2, all.size(), "findAll should return 2 widgets");

    }

    @Test
    void whenCreateAccountThenSuccess() {

        Account model = Account.builder()
                .id(2L)
                .name("credit1")
                .createdTime(Instant.now())
                .customerId(3L)
                .createdAt(Instant.now())
                .updatedAt(Instant.now().plusSeconds(2000))
                .build();


        AccountDto dto = AccountDto.builder()
                .id(2L)
                .name("credit1")
                .createdTime(Instant.now())
                .customerId(3L)
                .createdAt(Instant.now())
                .updatedAt(Instant.now().plusSeconds(2000))
                .build();

        CreateAccountDto createAccountDto =CreateAccountDto.builder()
                .name("credit1")
                .customerId(3L)
                .build();

        when(accountRepository.save(any())).thenReturn(model);

        when(accountMapper.modelToDto(any())).thenReturn(dto);

        AccountDto serviceAccount = accountService.createAccount(createAccountDto);

        assertThat(serviceAccount.name()).isEqualTo("credit1");

    }
}


















