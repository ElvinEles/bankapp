package com.bank.app.account.controller;

import com.bank.app.account.dto.AccountDto;
import com.bank.app.account.dto.CreateAccountDto;
import com.bank.app.account.service.AccountService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.Instant;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
class AccountControllerTest {

    @Mock
    private AccountService accountService;

    private MockMvc mockMvc;

    @InjectMocks
    private ObjectMapper objectMapper;

    @BeforeEach
    void setup() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(new AccountController(accountService)).build();
    }


    @Test
    void whenGetAccountByIdThenSuccess() throws Exception {

        AccountDto dto = AccountDto.builder()
                .id(1L)
                .name("credit")
                .createdTime(Instant.now())
                .customerId(2L)
                .createdAt(Instant.now())
                .updatedAt(Instant.now().plusSeconds(2000))
                .build();

        when(accountService.getAccount(anyLong())).thenReturn(dto);

        mockMvc.perform(get("/api/v2/account/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("credit"));
    }


    @Test
    void whenGetAccountsThenSuccess() throws Exception {

        AccountDto dto1 = AccountDto.builder()
                .id(1L)
                .name("credit")
                .createdTime(Instant.now())
                .customerId(2L)
                .createdAt(Instant.now())
                .updatedAt(Instant.now().plusSeconds(2000))
                .build();

        AccountDto dto2 = AccountDto.builder()
                .id(2L)
                .name("credit1")
                .createdTime(Instant.now())
                .customerId(3L)
                .build();

        when(accountService.getAll()).thenReturn(List.of(dto1, dto2));

        mockMvc.perform(get("/api/v2/account")
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void whenCreateAccountThenSuccess() throws Exception {

        AccountDto dto = AccountDto.builder()
                .id(2L)
                .name("credit1")
                .createdTime(Instant.now())
                .customerId(3L)
                .createdAt(Instant.now())
                .updatedAt(Instant.now().plusSeconds(2000))
                .build();

        when(accountService.createAccount(any())).thenReturn(dto);

        CreateAccountDto createAccountDto =CreateAccountDto.builder()
                .name("credit1")
                .customerId(3L)
                .build();

        mockMvc.perform(post("/api/v2/account")
                        .content(objectMapper.writeValueAsString(createAccountDto))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.customerId").value(3L));

    }
}














