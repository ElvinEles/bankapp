package com.bank.app.user.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.bank.app.user.dto.UserDto;
import com.bank.app.user.email.EmailService;
import com.bank.app.user.mapper.UserMapper;
import com.bank.app.user.model.Role;
import com.bank.app.user.model.User;
import com.bank.app.user.repository.RoleRepository;
import com.bank.app.user.repository.UserRepository;
import com.bank.app.user.security.jwt.JwtService;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;


@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @InjectMocks
    private UserService userService;

    @Mock
    private UserRepository userRepository;
    @Mock
    private RoleRepository roleRepository;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private JwtService jwtService;
    @Mock
    private UserMapper userMapper;
    @Mock
    private EmailService emailService;


    @Test
    void whenGetUserByIdThenSuccess() {

        Role role = Role.builder()
                .id(1L)
                .name("USER")
                .build();

        User user = User.builder()
                .id(1L)
                .name("Elvin")
                .surname("Elesgerov")
                .password(passwordEncoder.encode("password"))
                .email("eles.elvin@gmail.com")
                .phone("+994775404544")
                .createdAt(ZonedDateTime.now(ZoneId.of("Asia/Baku")).toInstant())
                .updatedAt(ZonedDateTime.now(ZoneId.of("Asia/Baku")).toInstant())
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(false)
                .accountNonExpired(true)
                .activateToken("generatedcode")
                .roles(List.of(role))
                .build();

        when(userRepository.findById(anyLong())).thenReturn(Optional.ofNullable(user));

        UserDto userDto = UserDto.builder()
                .id(1L)
                .name("Elvin")
                .surname("Elesgerov")
                .email("eles.elvin@gmail.com")
                .phone("+994775404544")
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(false)
                .accountNonExpired(true)
                .build();

        when(userMapper.modelToDto(any())).thenReturn(userDto);

        UserDto serviceUser = userService.getUser(1L);

        //assert
        assertThat(serviceUser.email()).isEqualTo("eles.elvin@gmail.com");

        //verify method worked
        verify(userRepository, times(1)).findById(anyLong());
        verify(userMapper, times(1)).modelToDto(any());

    }

    @Test
    void whenGetUserByIdWhenNoFoundIdThenException() {
        when(userRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThatThrownBy(() -> userService.getUser(anyLong()))
                .isInstanceOf(UsernameNotFoundException.class);
    }


}














