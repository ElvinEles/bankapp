package com.bank.app.user.exception;

public class RoleNotNotFound extends RuntimeException{

    public RoleNotNotFound(String message) {
        super(message);
    }
}
