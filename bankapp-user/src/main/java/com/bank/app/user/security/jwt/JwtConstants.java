package com.bank.app.user.security.jwt;

public final class JwtConstants {

    public static String USER_ID = "id";
    public static String USER_EMAIL = "email";
    public static String USER_ROLES = "roles";

}
