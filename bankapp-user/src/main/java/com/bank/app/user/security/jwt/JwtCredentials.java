package com.bank.app.user.security.jwt;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Builder
@AllArgsConstructor
@Getter
public class JwtCredentials {

    private Long id;
    private String email;
    private List<String> roles;
}
