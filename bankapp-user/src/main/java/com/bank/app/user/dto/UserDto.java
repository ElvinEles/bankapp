package com.bank.app.user.dto;

import lombok.Builder;

import java.io.Serial;
import java.io.Serializable;

@Builder
public record UserDto(
        Long id,
        String name,
        String surname,
        String email,
        String phone,
        boolean enabled,
        boolean accountNonExpired,
        boolean accountNonLocked,
        boolean credentialsNonExpired
) implements Serializable {
}
