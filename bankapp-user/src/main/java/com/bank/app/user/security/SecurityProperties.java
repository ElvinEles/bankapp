package com.bank.app.user.security;

import lombok.Data;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;

@Configuration
@ConfigurationProperties(prefix = "security.cors")
@Slf4j
@Getter
public class SecurityProperties {

    private final CorsConfiguration cors= new CorsConfiguration();

}
