package com.bank.app.user.dto;

import lombok.Value;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Value
public class LoginRequestDto {


    @NotBlank
    @Email(message = "email is not correct")
    String email;

    @NotBlank
    @Size(min = 4, max = 10, message = "the password length should min 4 max 10")
    String password;
}
