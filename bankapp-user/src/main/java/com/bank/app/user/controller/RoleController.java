package com.bank.app.user.controller;

import com.bank.app.user.dto.RoleDto;
import com.bank.app.user.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v2/role")
public class RoleController {

    private final RoleService roleService;

    @PostMapping
    public ResponseEntity<String> saveRole(@RequestBody @Valid RoleDto roleDto) {
        roleService.saveRole(roleDto);
        return ResponseEntity.status(HttpStatus.OK).body("role is saved");
    }

    @GetMapping
    public ResponseEntity<String> get() {
        return ResponseEntity.status(HttpStatus.OK).body("role is received");
    }
}
