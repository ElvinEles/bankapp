package com.bank.app.user.mapper;

import com.bank.app.user.dto.UpdateDto;
import com.bank.app.user.dto.UserDto;
import com.bank.app.user.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;


@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserMapper {

    User dtoToModel(UpdateDto updateDto);

    UserDto modelToDto(User user);
}
