package com.bank.app.user.dto;


import lombok.AllArgsConstructor;
import lombok.Value;

import javax.validation.constraints.NotBlank;

@Value
@AllArgsConstructor
public class RoleDto {

    @NotBlank(message = "role name cannot be blank")
    String name;
}
