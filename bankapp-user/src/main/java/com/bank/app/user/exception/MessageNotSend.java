package com.bank.app.user.exception;

public class MessageNotSend extends RuntimeException{

    public MessageNotSend(String message) {
        super(message);
    }
}
