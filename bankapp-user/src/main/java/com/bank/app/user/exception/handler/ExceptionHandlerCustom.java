package com.bank.app.user.exception.handler;

import com.bank.app.user.exception.MessageNotSend;
import com.bank.app.user.exception.NotPermission;
import com.bank.app.user.exception.PasswordNotMatches;
import com.bank.app.user.exception.PhoneNumberException;
import com.bank.app.user.exception.RoleNotNotFound;
import com.bank.app.user.exception.UserExistByEmail;
import com.bank.app.user.internationalization.Message;
import com.twilio.exception.ApiException;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@RequiredArgsConstructor
public class ExceptionHandlerCustom {

    private static final String ERROR_PHONE = "error.phone";
    private static final String ERROR_PHONE_CORRECT = "error.phone.correct";
    private static final String USER_EXIST = "user.exist";
    private static final String USER_NOT_FOUND = "user.notfound";
    private static final String PASSWORD_NOT_MATCH = "password.notmatch";
    private static final String NOT_PERMISSION = "permission.not";
    private final Message messageSource;

    @ExceptionHandler(PhoneNumberException.class)
    public ResponseEntity<Map<String, Object>> phoneNumberException(PhoneNumberException exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(mapErrors(messageSource.toLocale(ERROR_PHONE), 400));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Map<String, Object>> methodArgumentException(MethodArgumentNotValidException exception) {
        Map<String, Object> errors = new HashMap<>();
        errors.put("timestamp", Instant.now().toString());
        errors.put("status", 400);

        for (FieldError error : exception.getFieldErrors()) {
            errors.put(error.getField(), error.getDefaultMessage());
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errors);
    }

    @ExceptionHandler(ApiException.class)
    public ResponseEntity<Map<String, Object>> responseApiException(ApiException exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(mapErrors(messageSource.toLocale(ERROR_PHONE_CORRECT), 400));
    }

    @ExceptionHandler(UserExistByEmail.class)
    public ResponseEntity<Map<String, Object>> userExist(UserExistByEmail exception) {
        return ResponseEntity.status(HttpStatus.CONFLICT)
                .body(mapErrors(exception.getMessage() + " " + messageSource.toLocale(USER_EXIST), 400));
    }


    @ExceptionHandler(UsernameNotFoundException.class)
    public ResponseEntity<Map<String, Object>> userNotFound(UsernameNotFoundException exception) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(mapErrors(exception.getMessage() + " " + messageSource.toLocale(USER_NOT_FOUND), 400));
    }

    @ExceptionHandler(RoleNotNotFound.class)
    public ResponseEntity<Map<String, Object>> roleNotFound(RoleNotNotFound exception) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(mapErrors("role is not found", 400));
    }

    @ExceptionHandler(PasswordNotMatches.class)
    public ResponseEntity<Map<String, Object>> passwordNotMatches(PasswordNotMatches exception) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                .body(mapErrors(messageSource.toLocale(PASSWORD_NOT_MATCH), 401));
    }

    @ExceptionHandler(NotPermission.class)
    public ResponseEntity<Map<String, Object>> notPermission(NotPermission exception) {
        return ResponseEntity.status(HttpStatus.FORBIDDEN)
                .body(mapErrors(messageSource.toLocale(NOT_PERMISSION), 403));
    }

    @ExceptionHandler(MessageNotSend.class)
    public ResponseEntity<Map<String, Object>> roleNotFound(MessageNotSend exception) {
        return ResponseEntity.status(HttpStatus.BAD_GATEWAY)
                .body(mapErrors("message could not send", 502));
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<Map<String, Object>> missingParam(MissingServletRequestParameterException exception) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(mapErrors(exception.getMessage(), 400));
    }


    private Map<String, Object> mapErrors(String message, Integer code) {
        Map<String, Object> errors = new HashMap<>();
        errors.put("timestamp", Instant.now().toString());
        errors.put("status", code);
        errors.put("error", message);
        return errors;
    }

}
