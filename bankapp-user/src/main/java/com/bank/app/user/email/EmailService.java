package com.bank.app.user.email;

import com.bank.app.user.exception.MessageNotSend;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailSendException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Component
@RequiredArgsConstructor
public class EmailService {

    private final JavaMailSender javaMailSender;

    @Value("${spring.mail.username}")
    private String senderEmail;

    @Value("${email.activate.link}")
    private String activateLink;


    public void sendMail(String sendToEmail, String token) {

        final MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        final MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, "utf-8");


        try {
            helper.setTo(sendToEmail);
            helper.setSubject("BankApp - Account Activation");
            helper.setText("To activate your BankApp account, please click on this link below \n " + activateLink + token);
            helper.setFrom(senderEmail);
            javaMailSender.send(mimeMessage);
        } catch (MessagingException | MailSendException exception) {
            throw new MessageNotSend("the message could not be sent the reason: " + exception.getMessage());
        }
    }
}
