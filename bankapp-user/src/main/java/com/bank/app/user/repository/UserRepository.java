package com.bank.app.user.repository;

import com.bank.app.user.model.User;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    @EntityGraph(value = "User.withRoles", type = EntityGraph.EntityGraphType.FETCH, attributePaths = {"roles"})
    Optional<User> findByEmail(String email);

    @Override
    @EntityGraph(value = "User.withRoles", type = EntityGraph.EntityGraphType.FETCH, attributePaths = {"roles"})
    List<User> findAll();


    @Override
    @EntityGraph(value = "User.withRoles", type = EntityGraph.EntityGraphType.FETCH, attributePaths = {"roles"})
    Optional<User> findById(Long aLong);

    @Transactional
    @Modifying
    @Query(value = "UPDATE User u SET u.enabled = true WHERE u.activateToken = :token")
    int activateUser(@Param("token") String token);

    @EntityGraph(value = "User.withRoles", type = EntityGraph.EntityGraphType.FETCH, attributePaths = {"roles"})
    @Query(value = "SELECT u FROM User u WHERE u.activateToken = :token")
    Optional<User> checkActivateUser(@Param("token") String token);

    @Transactional
    @Modifying
    @Query(value = "UPDATE User u SET u.password = :updatedpassword WHERE u.id = :id")
    int update(Long id, String updatedpassword);
}
