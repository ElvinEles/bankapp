package com.bank.app.user.exception;

public class UserExistByEmail extends RuntimeException {

    public UserExistByEmail(String message) {
        super(message);
    }
}
