package com.bank.app.user.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public record UpdatePasswordDto(

        @NotBlank Long id,

        @NotBlank
        @Size(min = 4, max = 10, message = "the password length should min 4 max 10")
        String currentpassword,

        @NotBlank
        @Size(min = 4, max = 10, message = "the password length should min 4 max 10")
        String updatepassword,

        @NotBlank
        @Size(min = 4, max = 10, message = "the password length should min 4 max 10")
        String copyupdatepassword
) {
}
