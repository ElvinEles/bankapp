package com.bank.app.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@EnableCaching
//@EnableSwagger2
@SpringBootApplication
public class BankAppUserApplication {

    public static void main(String[] args) {
        SpringApplication.run(BankAppUserApplication.class, args);
    }

}
