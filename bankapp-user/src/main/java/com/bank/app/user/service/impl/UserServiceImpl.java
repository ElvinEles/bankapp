package com.bank.app.user.service.impl;

import com.bank.app.user.dto.*;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UserServiceImpl {

    void registerUser(RegisterDto registerDto);

    LoginResponseDto login(LoginRequestDto loginRequestDto);

    void updateUser(UpdateDto updateDto);

    UserDto getUser(Long id);

    void updateUserAdmin(UserDtoAdmin userDtoAdmin);

    List<UserDto> getUsers();

    ResponseEntity<String> activateUser(String token);

    ResponseEntity<String> updatePassword(UpdatePasswordDto passwordDto);
}
