package com.bank.app.user.constants;

public enum RoleType {
    USER,
    ADMIN
}
