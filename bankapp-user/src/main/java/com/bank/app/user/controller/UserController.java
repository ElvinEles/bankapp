package com.bank.app.user.controller;

import com.bank.app.user.dto.LoginRequestDto;
import com.bank.app.user.dto.LoginResponseDto;
import com.bank.app.user.dto.RegisterDto;
import com.bank.app.user.dto.UpdateDto;
import com.bank.app.user.dto.UpdatePasswordDto;
import com.bank.app.user.dto.UserAuth;
import com.bank.app.user.dto.UserDto;
import com.bank.app.user.dto.UserDtoAdmin;
import com.bank.app.user.exception.NotPermission;
import com.bank.app.user.internationalization.Message;
import com.bank.app.user.service.UserService;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.validation.Valid;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/api/v1/user")
public class UserController {

    private final UserService userService;

    private final Message message;

    @GetMapping("/permit/lang")
    public String getMessage(@RequestParam("msg") String msg) {
        return message.toLocale(msg);
    }


    @PostMapping("/permit/register")
    public ResponseEntity<String> register(@Valid @RequestBody RegisterDto registerDto) {
        userService.registerUser(registerDto);
        return ResponseEntity.status(HttpStatus.OK).body("user is registered");
    }

    @PostMapping("/permit/login")
    public ResponseEntity<LoginResponseDto> login(@Valid @RequestBody LoginRequestDto loginRequestDto) {
        return ResponseEntity.status(HttpStatus.OK).body(userService.login(loginRequestDto));
    }

    @GetMapping("/permit/activate")
    public ResponseEntity<String> activateUser(@RequestParam String token) {
        return userService.activateUser(token);
    }

    @PutMapping("/auth/update/password")
    public ResponseEntity<String> updatePassword(@RequestBody UpdatePasswordDto passwordDto) {
        return userService.updatePassword(passwordDto);
    }


    @GetMapping("/auth/{id}")
    public ResponseEntity<UserDto> getUser(@PathVariable Long id) {
        UserAuth auth = (UserAuth) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (!Objects.equals(auth.id(), id)) {
            throw new NotPermission();
        }
        return ResponseEntity.status(HttpStatus.OK).body(userService.getUser(id));
    }


    @PutMapping("/auth/update")
    public ResponseEntity<Map<String, Object>> updateUser(@Valid @RequestBody UpdateDto updateDto) {

        UserAuth auth = (UserAuth) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (!Objects.equals(auth.id(), updateDto.id())) {
            throw new NotPermission();
        }
        userService.updateUser(updateDto);
        return ResponseEntity.ok(responseMap(200, "user is updated"));
    }


    @PutMapping("/auth/admin/update")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Map<String, Object>> updateUserAdmin(@Valid @RequestBody UserDtoAdmin userDtoAdmin) {
        userService.updateUserAdmin(userDtoAdmin);
        return ResponseEntity.ok(responseMap(200, "user is updated"));
    }


    @GetMapping("/auth/users")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<List<UserDto>> getUsers() {
        return ResponseEntity.ok(userService.getUsers());
    }

    private Map<String, Object> responseMap(Integer status, String message) {
        Map<String, Object> success = new HashMap<>();
        success.put("timestamp", Instant.now().toString());
        success.put("status", status);
        success.put("success", message);
        return success;
    }
}

















