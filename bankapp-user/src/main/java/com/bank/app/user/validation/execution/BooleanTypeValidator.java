package com.bank.app.user.validation.execution;


import com.bank.app.user.validation.annotation.NotEmptyBoolean;
import org.springframework.beans.factory.annotation.Configurable;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Configurable
public class BooleanTypeValidator implements ConstraintValidator<NotEmptyBoolean, Boolean> {


    @Override
    public boolean isValid(Boolean value, ConstraintValidatorContext context) {
        return value != null;
    }
}
