package com.bank.app.user.validation.annotation;

import com.bank.app.user.validation.execution.BooleanTypeValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = BooleanTypeValidator.class)
public @interface NotEmptyBoolean {

    String message() default "doesn't seem to be a valid boolean data type";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
