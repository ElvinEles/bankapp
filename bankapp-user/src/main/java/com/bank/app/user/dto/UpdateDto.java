package com.bank.app.user.dto;

import com.bank.app.user.validation.annotation.NotEmptyLong;
import com.bank.app.user.validation.annotation.ValidPhoneNumber;

import javax.validation.constraints.NotBlank;


public record UpdateDto(@NotEmptyLong Long id,
                        @NotBlank String name,
                        @NotBlank String surname,
                        @NotBlank String email,
                        @NotBlank @ValidPhoneNumber(message = "Please enter a valid phone number") String phone) {

}
