package com.bank.app.user.internationalization;

import java.util.Locale;
import lombok.RequiredArgsConstructor;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class Message {

    private final ResourceBundleMessageSource messageSource;

    public String toLocale(String msgcode) {
        Locale locale = LocaleContextHolder.getLocale();
        return messageSource.getMessage(msgcode, null, locale);
    }
}
