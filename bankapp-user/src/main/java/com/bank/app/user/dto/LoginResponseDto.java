package com.bank.app.user.dto;


import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class LoginResponseDto {

    private String jwt;
}
