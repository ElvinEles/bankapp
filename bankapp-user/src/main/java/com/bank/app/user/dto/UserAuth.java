package com.bank.app.user.dto;

import lombok.Builder;

@Builder
public record UserAuth(
        Long id,
        String email
) {
}
