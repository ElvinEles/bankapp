package com.bank.app.user.dto;


import com.bank.app.user.validation.annotation.ValidPhoneNumber;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


public record RegisterDto(

        @NotBlank
        @Pattern(regexp = "^[A-Za-z]+")
        String name,

        @NotBlank
        @Pattern(regexp = "^[A-Za-z]+")
        String surname,

        @NotBlank
        @Size(min = 4, max = 10, message = "the password length should min 4 max 10")
        String password,

        @NotBlank
        @Email(message = "email is not correct")
        String email,


        @ValidPhoneNumber()
        String phone
) {
}
