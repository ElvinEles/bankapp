package com.bank.app.user.exception;

public class UserNotActivated extends RuntimeException{

    public UserNotActivated(String message) {
        super(message);
    }
}
