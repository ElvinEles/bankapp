package com.bank.app.user.service;

import com.bank.app.user.dto.RoleDto;
import com.bank.app.user.model.Role;
import com.bank.app.user.repository.RoleRepository;
import com.bank.app.user.service.impl.RoleServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.ZoneId;
import java.time.ZonedDateTime;

@Service
@RequiredArgsConstructor
public class RoleService implements RoleServiceImpl {

    private final RoleRepository roleRepository;


    @Override
    public void saveRole(RoleDto roleDto) {
        roleRepository.save(Role.builder()
                .name(roleDto.getName().toUpperCase())
                .createdAt(ZonedDateTime.now(ZoneId.of("Asia/Baku")).toInstant())
                .updatedAt(ZonedDateTime.now(ZoneId.of("Asia/Baku")).toInstant())
                .build());
    }
}
