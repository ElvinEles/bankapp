package com.bank.app.user.service.impl;

import com.bank.app.user.dto.RoleDto;

public interface RoleServiceImpl {

    void saveRole(RoleDto roleDto);
}
