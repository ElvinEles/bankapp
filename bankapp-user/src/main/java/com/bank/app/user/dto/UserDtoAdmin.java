package com.bank.app.user.dto;

import com.bank.app.user.validation.annotation.NotEmptyBoolean;
import com.bank.app.user.validation.annotation.NotEmptyLong;

import javax.validation.constraints.NotNull;


public record UserDtoAdmin(

        @NotEmptyLong
        Long id,

        @NotEmptyBoolean
        boolean enabled,

        @NotEmptyBoolean
        boolean accountNonExpired,

        @NotEmptyBoolean
        boolean accountNonLocked,

        @NotEmptyBoolean
        @NotNull
        boolean credentialsNonExpired

) {
}
