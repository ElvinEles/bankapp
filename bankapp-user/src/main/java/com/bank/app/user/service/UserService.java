package com.bank.app.user.service;


import com.bank.app.user.dto.LoginRequestDto;
import com.bank.app.user.dto.LoginResponseDto;
import com.bank.app.user.dto.RegisterDto;
import com.bank.app.user.dto.UpdateDto;
import com.bank.app.user.dto.UpdatePasswordDto;
import com.bank.app.user.dto.UserDto;
import com.bank.app.user.dto.UserDtoAdmin;
import com.bank.app.user.email.EmailService;
import com.bank.app.user.exception.PasswordNotMatches;
import com.bank.app.user.exception.UserDetailsChecker;
import com.bank.app.user.exception.UserExistByEmail;
import com.bank.app.user.exception.UserNotActivated;
import com.bank.app.user.mapper.UserMapper;
import com.bank.app.user.model.Role;
import com.bank.app.user.model.User;
import com.bank.app.user.repository.RoleRepository;
import com.bank.app.user.repository.UserRepository;
import com.bank.app.user.security.jwt.JwtCredentials;
import com.bank.app.user.security.jwt.JwtService;
import com.bank.app.user.service.impl.UserServiceImpl;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserService implements UserServiceImpl {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final UserMapper userMapper;
    private final EmailService emailService;

    private final UserDetailsChecker userDetailsChecker;


    @Override
    public void registerUser(RegisterDto registerDto) {

        Optional<User> userDb = userRepository.findByEmail(registerDto.email().trim());

        if (userDb.isPresent()) {
            throw new UserExistByEmail(registerDto.email());
        }


        Role role = roleRepository.findByName("USER")
                .orElseThrow(() -> new RuntimeException("role is not found"));

        String activateToken = generateToken();


        User user = User.builder()
                .name(registerDto.name().toLowerCase().trim())
                .surname(registerDto.surname().toLowerCase().trim())
                .password(passwordEncoder.encode(registerDto.password().toLowerCase().trim()))
                .email(registerDto.email().toLowerCase().trim())
                .phone(registerDto.phone().toLowerCase().trim())
                .createdAt(ZonedDateTime.now(ZoneId.of("Asia/Baku")).toInstant())
                .updatedAt(ZonedDateTime.now(ZoneId.of("Asia/Baku")).toInstant())
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(false)
                .accountNonExpired(true)
                .activateToken(activateToken)
                .roles(List.of(role))
                .build();


        userRepository.save(user);

        //sending uuid token for activate user
        emailService.sendMail(registerDto.email().trim(), activateToken);

    }

    private String generateToken() {
        return UUID.randomUUID().toString();
    }

    @Override
    public LoginResponseDto login(LoginRequestDto loginRequestDto) {

        User userDb = userRepository.findByEmail(loginRequestDto.getEmail())
                .orElseThrow(() -> new UsernameNotFoundException(loginRequestDto.getEmail()));


        boolean matches = passwordEncoder.matches(loginRequestDto.getPassword(), userDb.getPassword());

        if (!matches) {
            throw new PasswordNotMatches();
        }

        userDetailsChecker.check(userDb);

        return LoginResponseDto.builder()
                .jwt(jwtService.generateToken(JwtCredentials.builder()
                        .id(userDb.getId())
                        .email(userDb.getEmail())
                        .roles(userDb.getRoles().stream()
                                .map(Role::getName)
                                .toList())
                        .build()))
                .build();
    }

    @Caching(evict = {
            @CacheEvict(cacheNames = "user", allEntries = true),
            @CacheEvict(cacheNames = "users", allEntries = true)
    })
    @Override
    public void updateUser(UpdateDto updateDto) {
        User user = userRepository.findById(updateDto.id())
                .orElseThrow(() -> new UsernameNotFoundException("not found such a user"));

        Optional<User> userDb = userRepository.findByEmail(updateDto.email().trim());

        if (userDb.isPresent()) {
            throw new UserExistByEmail(updateDto.email() + " already exist");
        }

        user.setName(updateDto.name());
        user.setSurname(updateDto.surname());
        user.setEmail(updateDto.email());
        user.setPhone(updateDto.phone());
        user.setUpdatedAt(ZonedDateTime.now(ZoneId.of("Asia/Baku")).toInstant());

        userRepository.save(user);

    }

    @Cacheable(cacheNames = "user", key = "#id")
    @Override
    public UserDto getUser(Long id) {

        User user = userRepository.findById(id)
                .orElseThrow(() -> new UsernameNotFoundException("not found such a user"));
        log.info("user for getting {}", user);
        return userMapper.modelToDto(user);
    }

    @Caching(evict = {
            @CacheEvict(cacheNames = "user", allEntries = true),
            @CacheEvict(cacheNames = "users", allEntries = true)
    })
    @Override
    public void updateUserAdmin(UserDtoAdmin userDtoAdmin) {
        User user = userRepository.findById(userDtoAdmin.id())
                .orElseThrow(() -> new UsernameNotFoundException("not found such a user"));

        user.setEnabled(userDtoAdmin.enabled());
        user.setAccountNonExpired(userDtoAdmin.accountNonExpired());
        user.setAccountNonLocked(userDtoAdmin.accountNonLocked());
        user.setCredentialsNonExpired(userDtoAdmin.credentialsNonExpired());
        user.setUpdatedAt(ZonedDateTime.now(ZoneId.of("Asia/Baku")).toInstant());
        userRepository.save(user);
    }

    @Cacheable(cacheNames = "users")
    @Override
    public List<UserDto> getUsers() {
        return userRepository.findAll()
                .stream()
                .map(userMapper::modelToDto)
                .toList();
    }

    @Override
    public ResponseEntity<String> activateUser(String token) {

        User user = userRepository.checkActivateUser(token)
                .orElseThrow(() -> new UsernameNotFoundException("user is not found"));

        if (user.isEnabled()) {
            return ResponseEntity.status(HttpStatus.OK).body("user already is activated");
        }

        int result = userRepository.activateUser(token);

        if (result == 0) {
            throw new UserNotActivated("user is not activated");
        }
        return ResponseEntity.status(HttpStatus.OK).body("user  is activated");
    }

    @Override
    public ResponseEntity<String> updatePassword(UpdatePasswordDto passwordDto) {

        User user = userRepository.findById(passwordDto.id())
                .orElseThrow(() -> new UsernameNotFoundException("not found such a user"));

        boolean match = passwordEncoder.matches(passwordDto.currentpassword(), user.getPassword());

        //checking if password is correct
        if (!match) {
            throw new PasswordNotMatches();
        }

        //cheking if two fields of new password are they equal
        if (!passwordDto.updatepassword().equals(passwordDto.copyupdatepassword())) {
            throw new PasswordNotMatches();
        }

        String updatedpassword = passwordEncoder.encode(passwordDto.copyupdatepassword());

        int update = userRepository.update(user.getId(), updatedpassword);

        return update > 0 ? ResponseEntity.status(HttpStatus.OK).body("the password is updated")
                : ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("the password is not updated");
    }
}

















