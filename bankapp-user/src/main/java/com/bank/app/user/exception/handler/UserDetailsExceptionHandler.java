package com.bank.app.user.exception.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class UserDetailsExceptionHandler {

    @ExceptionHandler(LockedException.class)
    public ResponseEntity<Map<String, Object>> notActivated(LockedException exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(mapErrors(exception));
    }

    @ExceptionHandler(DisabledException.class)
    public ResponseEntity<Map<String, Object>> disabledAccount(DisabledException exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(mapErrors(exception));
    }

    @ExceptionHandler(AccountExpiredException.class)
    public ResponseEntity<Map<String, Object>> expiredAccount(AccountExpiredException exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(mapErrors(exception));
    }

    @ExceptionHandler(CredentialsExpiredException.class)
    public ResponseEntity<Map<String, Object>> expiredCredentials(CredentialsExpiredException exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(mapErrors(exception));
    }

    private Map<String, Object> mapErrors(Exception exception) {
        Map<String, Object> errors = new HashMap<>();
        errors.put("timestamp", Instant.now().toString());
        errors.put("status", 400);
        errors.put("error", exception.getMessage());
        return errors;
    }
}
