package com.bank.app.user.security;

import com.bank.app.user.constants.RoleType;
import com.bank.app.user.security.jwt.JwtRequestFilter;
import javax.sql.DataSource;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
@RequiredArgsConstructor
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration {

    private final SecurityProperties securityProperties;
    private final JwtRequestFilter jwtRequestFilter;
    private final DataSource dataSource;

    private static final String SWAGGER2 = "/v2/api-docs";
    private static final String SWAGGER3 = "/v3/api-docs";
    private static final String SWAGGER_UI = "/swagger-ui/**";
    private static final String SWAGGER_HTML = "/swagger-ui.html";
    private static final String ACTUATOR = "/actuator/**";
    private static final String USER = "/api/v1/user/permit/**";
    private static final String USER_AUTH = "/api/v1/user/auth/**";
    private static final String ROLE = "/api/v2/role/**";
    private static final String SECRET = "remmbermetoken";

    @Bean
    public SecurityFilterChain httpSecurity(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.cors().configurationSource(configurationSource());
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.exceptionHandling().authenticationEntryPoint(new SecurityEntryPoint());
        http.exceptionHandling().accessDeniedHandler(new SecurityAccessDenied());
        //  http.requiresChannel().anyRequest().requiresSecure(); //forcing all request to HTTPS
        http.authorizeRequests().antMatchers(ACTUATOR).permitAll();
//        http.authorizeRequests().antMatchers(SWAGGER2).permitAll();
//        http.authorizeRequests().antMatchers(SWAGGER3).permitAll();
//        http.authorizeRequests().antMatchers(SWAGGER_HTML).permitAll();
//        http.authorizeRequests().antMatchers(SWAGGER_UI).permitAll();
        http.authorizeRequests().antMatchers(USER).permitAll();
        http.authorizeRequests().antMatchers(USER_AUTH).hasAnyAuthority(RoleType.ADMIN.name(), RoleType.USER.name());
        http.authorizeRequests().antMatchers(ROLE).hasAuthority(RoleType.ADMIN.name());
        http.authorizeRequests().anyRequest().authenticated();
        http.rememberMe()
                .key(SECRET)
                .rememberMeCookieName("remmber-me-cokkie")
                .rememberMeParameter("remmber-me")
                .tokenRepository(persistentTokenRepository())
                .tokenValiditySeconds(86400);
        http.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);

        return http.build();
    }


    protected CorsConfigurationSource configurationSource() {
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        final CorsConfiguration config = securityProperties.getCors();
        source.registerCorsConfiguration("/**", config);
        return source;
    }


    @Bean
    public PersistentTokenRepository persistentTokenRepository() {
        final JdbcTokenRepositoryImpl tokenRepository = new JdbcTokenRepositoryImpl();
        tokenRepository.setDataSource(dataSource);
        return tokenRepository;
    }


}
