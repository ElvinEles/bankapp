package com.bank.app.user.validation.annotation;

import com.bank.app.user.validation.execution.LongTypeValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = LongTypeValidator.class)
public @interface NotEmptyLong {

    String message() default "doesn't seem to be a valid long data type";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
